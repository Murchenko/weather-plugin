
import { IconsPlugin, AlertPlugin, CardPlugin, ListGroupPlugin, ImagePlugin, FormInputPlugin, FormPlugin, ButtonPlugin } from 'bootstrap-vue'
import Vue from 'vue'

function init () {
  // Install BootstrapVue
  Vue.use(IconsPlugin)
  Vue.use(CardPlugin)
  Vue.use(AlertPlugin)
  Vue.use(ListGroupPlugin)
  Vue.use(ImagePlugin)
  Vue.use(FormPlugin)
  Vue.use(FormInputPlugin)
  Vue.use(ButtonPlugin)
}

export default init
