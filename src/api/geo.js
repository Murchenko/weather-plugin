export default {
  getLocation (success, error) {
    if (!window.navigator.geolocation) {
      return {
        status: false,
        message: 'Geolocation is not supported by your browser'
      }
    }
    navigator.geolocation.getCurrentPosition(success, error)
    return {
      status: true,
      message: 'Locating...'
    }
  }
}
