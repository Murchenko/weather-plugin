import isJSON from '@/utils/isJson'

export default {
  getCacheCities () {
    const JSONCities = localStorage.getItem('cities-list')
    if (isJSON(JSONCities)) return JSON.parse(JSONCities)
    return []
  },
  addCacheCity (cityObject) {
    if (!cityObject) return null
    let currentCities = this.getCacheCities()
    if (!currentCities) currentCities = []
    currentCities.unshift(cityObject)
    currentCities = JSON.stringify(currentCities)
    localStorage.setItem('cities-list', currentCities)
  },
  reSaveCities (cities) {
    let saveValue = ''
    if (cities) saveValue = JSON.stringify(cities)
    localStorage.setItem('cities-list', saveValue)
  },
  removeCacheCity (index) {
    let currentCities = this.getCacheCities()
    currentCities.splice(index, 1)
    currentCities = JSON.stringify(currentCities)
    localStorage.setItem('cities-list', currentCities)
  }
}
