import axios from 'axios'

export default {
  getWeatherByCityName (cityName) {
    return axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${process.env.VUE_APP_API_KEY}`)
  },
  getWeatherByCoords (lat, long) {
    return axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&units=metric&appid=${process.env.VUE_APP_API_KEY}`)
  },
  getIconByWeatherCode (code) {
    return `http://openweathermap.org/img/wn/${code}@2x.png`
  }
}
