export default {
  isFirstVisit () {
    const isFirstVisit = localStorage.getItem('is-first-visit')
    if (!Number(isFirstVisit)) return true
    return false
  },
  setFirstVisit (value) {
    localStorage.setItem('is-first-visit', value)
  }
}
